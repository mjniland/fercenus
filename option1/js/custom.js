wow = new WOW(
      {
        boxClass:     'motion', 
        animateClass: 'animated',
        mobile:       false,
      }
    );
    wow.init();




    $(".grid-item").click(function(){
    	var itemId = $(this).attr("id");
    	$(".item").removeClass("service-active");
    	$("."+itemId).addClass("service-active");
    	$(".grid-item").removeClass("service-btn-active");
    	$(this).addClass("service-btn-active");
    })


    $(window).scroll(function(){
        var halfWidth = $(window).height() / 2;
        var winDis = $(window).scrollTop();
    	$(".content-box").each(function(){
            var disTop = $(this).offset().top - halfWidth;

            if (winDis > disTop){
                var idName = $(this).attr("id");
                $(".nav > ul > li").removeClass("nav-active");
                $("."+idName).addClass("nav-active");
            }
        })
    })