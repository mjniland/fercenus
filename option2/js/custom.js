wow = new WOW(
      {
        boxClass:     'motion', 
        animateClass: 'animated',
        mobile:       false,
      }
    );
    wow.init();

    $(document).ready(function(){
        $(".nav").hide();
    })

    $(window).scroll(function(){
        var halfWidth = $(window).height() / 2;
        var winDis = $(window).scrollTop();
    	$(".content-box").each(function(){
            var disTop = $(this).offset().top - halfWidth;

            if (winDis > disTop){
                var idName = $(this).attr("id");
                $(".nav-icons > a > div, .nav-item-active").removeClass("nav-active").removeClass("nav-item-active");
                $("."+idName).addClass("nav-active");
                $('[data-navItem='+idName+']').addClass("nav-item-active")
               changeImg();
            }
        })
    })

    window.setInterval(function(){
   y=0; 
},1500); 

    function changeImg() {
        var x = Math.floor((Math.random() * 5)+1);

        while (y == 0){
            $(".side-bar").css({
                "background":'url(images/right'+x+'.jpg) no-repeat center center',
                "background-size":"cover",
                "transition":"all 0.3s",
            })
            y++;
        }
        
    }


    $(".nav-icon").click(function(){
        $(".nav").slideDown();
        $(".nav-icon").fadeOut();
    })


    $(".nav > li").click(function(){
        $(".nav > li").removeClass("nav-item-active");
        $(this).addClass("nav-item-active");
    })

    $(".back").click(function(){
        $(".nav-icon").fadeIn();
        $(".nav").slideUp();
    });