wow = new WOW(
      {
        boxClass:     'motion', 
        animateClass: 'animated',
        mobile:       false,
      }
    );
    wow.init();


$(document).ready(function(){
    var winWidth = $(window).width();
    appendMe(winWidth)
})

$(window).resize(function(){
    var winWidth = $(window).width();
    appendMe(winWidth)
})

function appendMe(winWidth) {
     if (winWidth < 800){
        $(".append-me").appendTo(".append");
        $(".nav").hide();
        $(".nav-icon").css("display","inline-block");
    }
    else {
        $(".append-me").appendTo(".append-back");
        $(".nav").css("display","inline-block");
        $(".nav-icon").hide();
    }
}

$(".nav-icon").click(function(){
    $(this).hide();
    $(".nav").fadeIn();
})

$(".back").click(function(){
    $(".nav-icon").css("display","inline-block");
    $(".nav").slideUp();
})