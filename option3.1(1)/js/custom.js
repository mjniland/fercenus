wow = new WOW(
      {
        boxClass:     'motion', 
        animateClass: 'animated',
        mobile:       false,
      }
    );
    wow.init();


$(document).ready(function(){
    var winWidth = $(window).width();
    nav(winWidth)
})

$(window).resize(function(){
    var winWidth = $(window).width();
    nav(winWidth)
})

function nav(winWidth) {
     if (winWidth < 800){
        $(".nav").hide();
        $(".nav-icon").css("display","inline-block");
    }
    else {
        $(".nav").css("display","inline-block");
        $(".nav-icon").hide();
    }
}

$(".nav-icon").click(function(){
    $(this).hide();
    $(".nav").fadeIn();
})

$(".back").click(function(){
    $(".nav-icon").css("display","inline-block");
    $(".nav").slideUp();
})